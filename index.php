<?php
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "Nama Binatang : " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$frog = new Frog ("buduk");
echo "Nama Binatang : " . $frog -> name . "<br>";
echo "Jumlah Kaki : " . $frog -> legs. "<br>";
echo "Cold Blooded : " . $frog -> cold_blooded . "<br>";
echo "Jump : " .$frog -> jump . "<br><br>";

$ape = new Ape ("kera sakti");
echo "Nama Binatang : " . $ape -> name . "<br>";
echo "Jumlah Kaki : " . $ape -> kaki. "<br>";
echo "Cold Blooded : " . $ape -> cold_blooded . "<br>";
echo "Yell : " .$ape -> yell . "<br><br>";

?>